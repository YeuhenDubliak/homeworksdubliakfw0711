//1
//Створюємо масив
const numArr = [1, 4, 8, 9, 11, 23, 128, 28, 59, 743, 346, 347, 10];

//Функція сортування елементів масиву за зростанням
function arrSort(arr) {
  arr.sort(function (a, b) {
    return a - b;
  });
}
//Функція map з двома параметрами 1 - функція, яка буде виконувати дії з масивом, 2 - масив
function map(func, arr) {
  return func(arr);
}
//Виклик функції map
map(arrSort, numArr);
//Вивід в консоль відсортованого масиву numArr
console.log(numArr);

//2
//Запит на те, щоб користувач ввів свій вік
let enterAge = +prompt('Введите ваш возраст');

/*Функція виводить true якщо, вік користувача більше або дорівнює 18-ти рокам, 
і виводить запит на підтвердження якщо, користувачу менше 18 років */
function checkAge(age) {
  return age >= 18
    ? console.log(true)
    : console.log(confirm('Родители разрештли?'));
}
//Виклик функції checkAge
checkAge(enterAge);

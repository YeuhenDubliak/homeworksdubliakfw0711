const stop = document.querySelector('.first_input');
const cont = document.querySelector('.second_input');
const [...imgArr] = document.querySelectorAll('.image-to-show');

let i = 0;
let imgInterval;

function myInterval() {
  imgInterval = setInterval(() => {
    imgArr[i].style.display = 'none';
    i++;

    if (i >= imgArr.length) {
      i = 0;
    }

    imgArr[i].style.display = 'block';
  }, 10000);
}

window.onload = function () {
  myInterval();
};

stop.addEventListener('click', () => {
  clearInterval(imgInterval);
});

cont.addEventListener('click', () => {
  myInterval();
});

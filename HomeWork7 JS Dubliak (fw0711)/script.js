// 1 task -----------------------------------------------------------
// Создаем массив со словами
const words = ['программа', 'макака', 'прекрасный', 'оладушек'];
// Выбираем случайное слово
let word = words[Math.floor(Math.random() * words.length)];
// Создаем итоговый массив
const answerArray = [];
for (let i = 0; i < word.length; i++) {
  answerArray[i] = '_';
}
let remainingLetters = word.length;
// Игровой цикл
while (remainingLetters > 0) {
  // Показываем состояние игры
  alert(answerArray.join(' '));

  // Запрашиваем вариант ответа
  let guess = prompt('Угадайте букву, или нажмите Отмена для выхода из игры.');
  if (guess === null) {
    // Выходим из игрового цикла
    break;
  } else if (guess.length !== 1) {
    alert('Пожалуйста, введите одиночную букву.');
  } else {
    // Обновляем состояние игры
    for (let i = 0; i < word.length; i++) {
      if (word[j] === guess) {
        answerArray[i] = guess;
        remainingLetters--;
      }
    }
  }
  // Конец игрового цикла
}
// Отображаем ответ и поздравляем игрока
alert(answerArray.join(' '));
alert(`Отлично! Было загадано слово ${word}`);

// 2 task -----------------------------------------------------------
function CreateNewUser() {
  this.firstName = prompt('Enter your name');
  this.lastName = prompt('Enter your last name ');
}
CreateNewUser.prototype.getLogin = function () {
  let newName = this.firstName.charAt(0);
  return `Your new login - ${newName + this.lastName}`.toLowerCase();
};

const newUser1 = new CreateNewUser();
console.log(newUser1.getLogin());

// 3 task -----------------------------------------------------------
function CreateNewUser() {
  this.firstName = prompt('Enter your name');
  this.lastName = prompt('Enter your last name ');
  this.birthday = prompt('Enter your date of birth', 'dd.mm.yyyy');
}

CreateNewUser.prototype.getLogin = function () {
  let newName = this.firstName.charAt(0);
  return `Your new login - ${newName + this.lastName}`.toLowerCase();
};

CreateNewUser.prototype.getAge = function () {
  let yearOfBirth = parseInt(this.birthday.substr(6, 4));
  let currYear = new Date().getFullYear();
  return `You are ${currYear - yearOfBirth} years old`;
};

CreateNewUser.prototype.getPassword = function () {
  let firstLett = this.firstName.charAt(0).toUpperCase();
  let yearOfBirth = parseInt(this.birthday.substr(6, 4));
  return `Your new password ${
    firstLett + this.lastName.toLowerCase() + yearOfBirth
  }`;
};

const newUser2 = new CreateNewUser();
console.log(newUser2.getLogin());
console.log(newUser2.getAge());
console.log(newUser2.getPassword());

// 4 task -----------------------------------------------------------
function filterBy(filterArr, dataType) {
  const newArr = filterArr.filter((i) => typeof i !== dataType);
  return newArr;
}

const newArr = [
  12,
  24,
  'ffsdf',
  'sdfsdf',
  12,
  undefined,
  null,
  67,
  22,
  'sdfdf',
  'ffg',
];
console.log(newArr);
console.log(filterBy(newArr, 'string'));

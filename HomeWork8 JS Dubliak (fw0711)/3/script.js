//task
/*Создайте 5 дивов на странице затем используя getElementsByTagName и forEach поменяйте дивам цвет фона.*/

for (let i = 0; i <= 5; i++) {
  const itemDiv = document.createElement('div');
  itemDiv.innerText = 'Hello world';
  document.body.append(itemDiv);
}

const [...paintDiv] = document.getElementsByTagName('div');

paintDiv.forEach((element) => {
  element.style.backgroundColor = 'red';
});

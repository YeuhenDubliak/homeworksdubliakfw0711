const img = document.createElement('img');
const btn = document.createElement('button');

btn.innerText = 'Изменить картинку';
btn.style.display = 'block';

img.setAttribute('src', 'https://itproger.com/img/courses/1476977240.jpg');
img.style.width = '500px';
img.style.height = '300px';

document.body.append(img);
document.body.append(btn);

let link1 = 'https://itproger.com/img/courses/1476977240.jpg';
let link2 = 'https://itproger.com/img/courses/1476977488.jpg';
let link3 =
  'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png';

btn.addEventListener('click', () => {
  if (img.src === link1) {
    img.src = link2;
  } else if (img.src === link2) {
    img.src = link3;
  } else if (img.src === link3) {
    img.src = link1;
  }
});

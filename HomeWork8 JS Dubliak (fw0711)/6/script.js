window.onload = function () {
  const par = document.querySelectorAll('p');

  for (let i = 0; i < par.length; i++) {
    par[i].addEventListener('click', activeElement);
  }

  function activeElement() {
    this.classList.toggle('active_element');
  }
};

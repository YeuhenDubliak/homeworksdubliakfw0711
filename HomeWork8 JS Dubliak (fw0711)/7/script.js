const userInput = document.querySelector('input');
const [...btnsArr] = document.querySelectorAll('button');

btnsArr.forEach((index) => {
  index.addEventListener('click', () => {
    if (index.classList.contains('plus_btn')) {
      userInput.value = userInput.value + '+';
    } else if (index.classList.contains('minus_btn')) {
      userInput.value = userInput.value + '-';
    } else if (index.classList.contains('slash_btn')) {
      userInput.value = userInput.value + '/';
    } else if (index.classList.contains('star_btn')) {
      userInput.value = userInput.value + '*';
    } else if (index.innerText === '1') {
      userInput.value = userInput.value + '1';
    } else if (index.innerText === '2') {
      userInput.value = userInput.value + '2';
    } else if (index.innerText === '3') {
      userInput.value = userInput.value + '3';
    } else if (index.innerText === '4') {
      userInput.value = userInput.value + '4';
    } else if (index.innerText === '5') {
      userInput.value = userInput.value + '5';
    } else if (index.innerText === '6') {
      userInput.value = userInput.value + '6';
    } else if (index.innerText === '7') {
      userInput.value = userInput.value + '7';
    } else if (index.innerText === '8') {
      userInput.value = userInput.value + '8';
    } else if (index.innerText === '9') {
      userInput.value = userInput.value + '9';
    } else if (index.innerText === '0') {
      userInput.value = userInput.value + '0';
    }
  });
});

console.dir(btnsArr);

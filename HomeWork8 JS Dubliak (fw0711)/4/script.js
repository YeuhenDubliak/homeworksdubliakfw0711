//task

/*Создайте многострочное поле для ввода текста и кнопку. После нажатия кнопки пользователем 
приложение должно сгенерировать тег div с текстом который был в многострочном поле. многострочное 
поле следует очистить после переимещени информации*/

// Знаю что инлайн стили не правильно, но хотел попробовать создать все с помощью js
const textArea = document.createElement('textarea');
const header = document.createElement('p');
const btn = document.createElement('button');
const userInputDiv = document.createElement('div');

header.innerText = 'Введите ваш текст';
btn.innerText = 'Нажмите чтобы увидеть результат';

textArea.style.display = 'block';
textArea.style.marginBottom = '20px';
btn.style.marginBottom = '20px';

document.body.append(header);
document.body.append(textArea);
document.body.append(btn);

btn.addEventListener('click', () => {
  let temp = textArea.value;
  textArea.value = userInputDiv.innerText;
  userInputDiv.innerText = temp;
  textArea.value = '';
  document.body.append(userInputDiv);
});

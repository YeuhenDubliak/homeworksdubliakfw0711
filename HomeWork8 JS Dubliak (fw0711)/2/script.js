// task
/*Создайте 2 инпута и одну кнопку. Сделайте так чтобы инпуты обменивались содержимым.*/

const firstInput = document.querySelector('.first_input');
const secondInput = document.querySelector('.second_input');
const button = document.querySelector('.btn');

button.addEventListener('click', () => {
  let temp = firstInput.value;
  firstInput.value = secondInput.value;
  secondInput.value = temp;
});

//1
//Створення масиву styles
const styles = ['Джаз', 'Блюз'];
//Використання методу push() для додавання елементу в кінець масиву
styles.push('Рок-н-Ролл');
//Виведення результату в консоль
console.log(styles);

//2
//Створюємо змінну яка буде відповідати за середину нашого масиву
let middleIndex = Math.trunc(styles.length / 2);
//Цикл для заміни значення в середині масиву
for (let i = 0; i < styles.length; i++) {
  if (i == middleIndex) {
    styles.splice(i, 1, 'Классика');
  }
}
//Виводимо отриманий після всіх маніпуляцій масив
console.log(styles);

//3
//Використовуємо метод shift дл видаленян елементу з початку масиву, та виведення його в консоль
console.log(styles.shift());
//Виводимо новий масив styles
console.log(styles);

//4
//Використовуємо unshift для додавання двух нових елементів з початку масиву styles
styles.unshift('Рэп', 'Регги');
//Виводимо новий маасив styles
console.log(styles);

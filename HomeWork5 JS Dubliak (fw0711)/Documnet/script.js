const newDoc = {
  title: '',
  body: '',
  footer: '',
  date: '',
  complement: {
    compTitle: {
      title: '',
    },
    compBody: {
      body: '',
    },
    compFooter: {
      footer: '',
    },
    compDate: {
      date: '',
    },
  },

  readUserInput: function () {
    this.title = prompt('Enter document title');
    this.body = prompt('Enter document body');
    this.footer = prompt('Enter documnet footer');
    this.date = prompt('Enter document creating date');
    let conf = confirm('Do you want to add addition into your document?');
    if (conf === true) {
      this.complement.title = prompt('Enter addtion title');
      this.complement.body = prompt('Enter addtion body');
      this.complement.footer = prompt('Enter addtion footer');
      this.complement.date = prompt('Enter addtion creating date');
    }
  },
  writeUserInput: function () {
    document.write(`Document title: ${this.title} <br>`);
    document.write(`Document body: ${this.body} <br>`);
    document.write(`Document footer: ${this.footer} <br>`);
    document.write(`Document creating date: ${this.date} <br>`);
    if (
      this.complement.title.length != 0 &&
      this.complement.body.length != 0 &&
      this.complement.footer.length != 0 &&
      this.complement.date.length != 0
    ) {
      document.write(`Addition title: ${this.complement.title} <br>`);
      document.write(`Addition body: ${this.complement.body} <br>`);
      document.write(`Addition footer: ${this.complement.footer} <br>`);
      document.write(`Addition date: ${this.complement.date} <br>`);
    }
  },
};

newDoc.readUserInput();
newDoc.writeUserInput();

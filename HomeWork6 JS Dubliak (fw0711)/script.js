// first task

//Створюємо функцію конструктор Human
function Human(humanName, humanAge) {
  //Властивосі конструктора
  this.humanName = humanName;
  this.humanAge = humanAge;
}

//Метод конструктора для сортування масиву за властивістю вік людиин
Human.prototype.sortHumanArr = function (human) {
  return human.sort((prev, next) => prev.humanAge - next.humanAge);
};

//створюємо екземпляр функції-конструктора
let humansObj = new Human();

//створюємо масив
const humansArr = [];

//за допомогою методу push додаємо елементи в масив
humansArr.push(new Human('Andrew', 19));
humansArr.push(new Human('Anton', 11));
humansArr.push(new Human('Daria', 22));
humansArr.push(new Human('Maria', 15));
humansArr.push(new Human('Roman', 45));
humansArr.push(new Human('Yurii', 29));

//виводимо в консоль масив до сортування
console.log(humansArr);
//виводимо в консоль масив після сортування
console.log(humansObj.sortHumanArr(humansArr));

//second task
//Створюємо функцію конструктор SecHuman
function SecHuman(name, surname, age) {
  //Властивості
  this.name = name;
  this.surname = surname;
  this.age = age;
}

//Метод для перевірки віку стоворений в прототипі
SecHuman.prototype.checkAge = function () {
  if (this.age >= 18) {
    alert(`Nice to meet you ${this.name} ${this.surname}`);
  } else {
    alert(`Denied in entry, you are too young ${this.name} ${this.surname}`);
  }
};

//метод функції конструктору
SecHuman.createHuman = function () {
  return new SecHuman('Andres', 'Falinno', '21');
};

//Створення екземпляру
const human = new SecHuman();
human.name = 'David';
human.surname = 'Van';
human.age = '15';
human.checkAge();

SecHuman.createHuman().checkAge();
